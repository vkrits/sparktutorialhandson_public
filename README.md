# Hands On Spark Scala Applications #

This is a repository of Spark code examples presented during the [Spark Tutorial](http://www.ics.forth.gr/index_main.php?l=g&n=4&id=496).

Code Examples:

* *AverageZones* & 
* *MovieAnalyzer* (to be uploaded after the tutorial take place)