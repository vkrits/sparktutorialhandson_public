package org.test.spark

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext

object AverageZoneTemperature {

  def main(args: Array[String]) = {

    val conf = new SparkConf()
      .setAppName("AverageZoneTemperature")

    val sc = new SparkContext(conf)

    sc.setLogLevel("WARN")

    println("Average by country and zone")
    val baseRDD = sc.textFile("/home/vkrits/testdata/countryZoneTemp.txt")
      .map(x => x.split(","))
      .map(x => ((x(0), x(1)), x(2).trim().toInt))
      .mapValues((_, 1))
      // ((India,Z1),(22,1))
      // ((Australia,Z1),(29,1))
      // ((Australia,Z1),(24,1))
      // ((India,Z2),(24,1))
      // ((India,Z2),(28,1))
      .reduceByKey((x, y) => (x._1 + y._1, x._2 + y._2))
      // ((India,Z1),(22,1))
      // ((India,Z2),(52,2))
      // ((Australia,Z1),(53,2))
      //.mapValues { case (sum, count) => sum.toDouble / count } //toDouble is used otherwise it will return integers
      .mapValues((x) => x._1.toDouble / x._2)
      // ((Australia,Z1),26.5)
      // ((India,Z1),22.0)
      // ((India,Z2),26.0)*/
      .collect()
      //Try this using combineByKey!
      .foreach(println)
      
  }

}